/*******************
Justin Mulkin - 0952465
EGP 405-02
Lab 01: Hello Raknet
Assigned Thursday, August 31
Due Monday, September 4th at 9:29 am

�We certify that this work is entirely our own.  
The assessor of this project may reproduce this project 
and provide copies to other academic staff, and/or 
communicate a copy of this project to a plagiarism-checking 
service, which may retain a copy of the project on its database.�

Source file technically created by myself, however most of the code was
copied from the RakNet manual tutorials as per instruction of the
assignment

*********************/



#include <stdio.h>
//Including the raknet headers from the dev sdks
#include "RakNet/RakPeerInterface.h"
#include <iostream>

//Includes for part 2 of the tutorial
#include <string>
//Also a raknet header
#include "RakNet/MessageIdentifiers.h"

//Includes for part 3 of the tutorial
#include "RakNet/BitStream.h"
#include "RakNet\RakNetTypes.h" //MessageID

//Using the Raknet namespace for the classes
using namespace RakNet;

//Enum for gamemessages
enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	//Custom game message identifiers for the message exchange loop
	ID_GAME_MESSAGE_2,
	ID_GAME_MESSAGE_3
};

//Struct for the messages
#pragma pack(push, 1) //Pack the struct as byte-aligned (as per the creating packets webpage)
struct GameMessageStruct
{
	//Id for the type of message
	GameMessages messageType;

	//String of fixed size (Holds the desired message)
	char messageContent[128];
};
#pragma pack(pop)

//Main function
int main(void)
{
	//Main local variables for number of clients and the port of the server
	unsigned int maxClients;
	unsigned short serverPort;


	char str[512];
	RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
	bool isServer;

	//Making a raknet packet
	Packet *packet;

	printf("(C) or (S)erver?\n");

	//Changed deprecated function gets with fgets
	fgets(str, 512, stdin);

	//Getting the port number from the user
	printf("\n Enter the desired port number: ");

	scanf("%hi", &serverPort);

	if ((str[0] == 'c') || (str[0] == 'C'))
	{
		SocketDescriptor sd;
		peer->Startup(1, &sd, 1);
		isServer = false;
	}
	else
	{
		//Get the maximum number of clients from user input
		printf("\n Enter the max number of clients: ");

		//Scanning the input string with scanf
		scanf("%d", &maxClients);

		SocketDescriptor sd(serverPort, 0);
		peer->Startup(maxClients, &sd, 1);
		isServer = true;
	}

	/* Debug Print
	printf("%d\n",maxClients);
	printf("%d\n", serverPort);
	*/
	// TODO - Add code body here

	//If this is the sever
	if (isServer)
	{
		printf("Starting the server.\n");

		//Set the max number of incoming clients into the server
		peer->SetMaximumIncomingConnections(maxClients);
	}
	//Else if this is a client
	else
	{
		fgets(str, 512, stdin);
		printf("Enter server IP or hit enter for 127.0.0.1\n");

		//Get the string from the user
		fgets(str, 512, stdin);

		//If the user left blank for local
		if (str[0] == '\n' || str[0] == 0)
		{
			//Copy local IP into the string
			strcpy(str, "127.0.0.1");
		}

		printf("Starting the client.\n");
		//Start the client
		peer->Connect(str, serverPort, 0, 0);
		printf(str);
	}

	//Main loop for the program
	while (1)
	{
		for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
		{
			//Switch for the packet
			switch (packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				printf("Another client has disconnected.\n");
				break;
			case ID_REMOTE_CONNECTION_LOST:
				printf("Another client has lost the connection.\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				printf("Another client has connected.\n");
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Our connection request has been accepted.\n");

				//Part 3 default bitstream from tutorial
				// Use a BitStream to write a custom user message
				/*
				bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1); //Write game message 1 to the bitstream
				bsOut.Write("Hello world");
				
				RakNet::BitStream bsOut;
				//Custom message loop - part 1
				bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_2); //Write game message 1 to the bitstream
				bsOut.Write("Howdy partner!");

				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				*/

				//Part 3 using a struct instead of bitstream (kept the bitsream in but commented out just in case you wanted it)
				GameMessageStruct message; //Creating the struct for the message

				//Set the id type to game message 2
				message.messageType = ID_GAME_MESSAGE_2;

				//String to put into the struct
				char *msg = "Howdy Partner!\n";

				//Copying the message into the struct
				strcpy(message.messageContent, msg);
				//Casting the message as a char* and getting the sizeof to pass it along
				peer->Send((char*)&message, sizeof(message), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
				
				break;
			case ID_NEW_INCOMING_CONNECTION:
				printf("A connection is incoming.\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				printf("The server is full.\n");
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				if (isServer) {
					printf("A client has disconnected.\n");
				}
				else {
					printf("We have been disconnected.\n");
				}
				break;
			case ID_CONNECTION_LOST:
				if (isServer) {
					printf("A client lost the connection.\n");
				}
				else {
					printf("Connection lost.\n");
				}
				break;

				//Case for game message 1
			case ID_GAME_MESSAGE_1:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);
				printf("%s\n", rs.C_String());
			}
			break;
			//Cases for the other 2 game messages
			case ID_GAME_MESSAGE_2:
			{
				/* Bitstream, kept just in case
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);
				printf("%s\n", rs.C_String());
				RakNet::BitStream bsOut;
				//Custom message loop - part 2
				bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_3); //Write game message 3 to the bitstream
				bsOut.Write("Goodbye, partner...");

				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				*/

				//Print the previous message
				GameMessageStruct* prevMessage = (GameMessageStruct*)packet->data; //Get the message
				printf("%s", prevMessage->messageContent);

				//Part 3 using a struct instead of bitstream again
				GameMessageStruct message; //Creating the struct for the message

				//Set the id type to game message 3
				message.messageType = ID_GAME_MESSAGE_3;

				//String to put into the struct
				char *msg = "Goodbye, partner...\n";

				//Copying the message into the struct
				strcpy(message.messageContent, msg);
				//Casting the message as a char* and getting the sizeof to pass it along
				peer->Send((char*)&message, sizeof(message), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
				
				break;
			case ID_GAME_MESSAGE_3:
			{
				/* bitstream commented out again
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);
				printf("%s\n", rs.C_String());
				RakNet::BitStream bsOut;
				//Custom message loop - part 2
				bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_2); //Write game message 2 to the bitstream
				bsOut.Write("Howdy partner!");

				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				*/

				//Print the previous message
				GameMessageStruct* prevMessage = (GameMessageStruct*)packet->data; //Get the message
				printf("%s", prevMessage->messageContent);

				//Part 3 using a struct instead of bitstream for a third time
				GameMessageStruct message; //Creating the struct for the message
										   
				//Set the id type to game message 2
				message.messageType = ID_GAME_MESSAGE_2;

				//String to put into the struct
				char *msg = "Howdy Partner!\n";

				//Copying the message into the struct
				strcpy(message.messageContent, msg);
				//Casting the message as a char* and getting the sizeof to pass it along
				peer->Send((char*)&message, sizeof(message), HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
				break;
			default:
				printf("Message with identifier %i has arrived.\n", packet->data[0]);
				break;
			}		
		}
	}

	RakNet::RakPeerInterface::DestroyInstance(peer);

	return 0;
}